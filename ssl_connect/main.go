/**
Copyright (c) 2020, Nimblex Co .,Ltd.
Created on 2020-07-21 19:35
**/
package main

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

func main() {
	db, err := sql.Open("postgres", "user=test password=test dbname=test host=192.168.80.161 port=5433 sslmode=require sslcert=./memfiredb.crt sslkey=./memfiredb.key sslrootcert=./ca.crt")
	if err != nil {
		log.Fatal("数据库连接失败" + err.Error())
	}

	defer db.Close()
	rows, err := db.Query("SELECT * FROM table_name WHERE id = 1")
	if err != nil {
		log.Fatal(err.Error())
	}
	println(rows)
}
